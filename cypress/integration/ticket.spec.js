
describe('Tickets', () => {

    beforeEach(() =>{
        cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html");
    })

    it('Fills all the text input fields', () => {
        cy.get("#first-name").type("Teste");
        cy.get("#last-name").type("Teste 123");
        cy.get("#email").type("teste@teste.com.br");
        cy.get("#signature").type("Teste Teste 123");
    });

    it('Select two tickets', () => {
        cy.get("#ticket-quantity").select("2");
    });

    it('Select vip ticket type', () => {
       cy.get("#vip").check();
    });

    it('Selects Social Media Checkbox', () => {
        cy.get("#social-media").check();
    });

    it('Select friend and publication, then unched friend', () => {
        cy.get('#friend').check();
        cy.get('#publication').check();
        cy.get('#friend').uncheck();
    });

    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it('alert on invalid email', () => {
        cy.get("#email").as("email").type("teste.com.br");
        cy.get("#email.invalid").should("exist");

        cy.get("@email").clear().type("teste@teste.com.br");
        cy.get("#email.invalid").should("not.exist");
    });

    it('fills and reset the form', () => {
        const firstName = "Teste";
        const lastName = "Teste 123";
        const fullName = `${firstName} ${lastName}`

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("teste@teste.com.br");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get('#friend').check();
        cy.get('#publication').check();
        cy.get("#requests").type("Teste e2e");
        
        cy.get(".agreement p").should(
            "contain", 
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        );

        cy.get('#agree').check();
        cy.get("#signature").type(fullName);

        cy.get('[type="submit"]').as("submitButton").should("not.be.disabled");

        cy.get('.reset').click();
    });

    it('fills mandatory fields using support command', () => {
        const customer = {
            firstName: "João",
            lastName: "Silva",
            email: "joaosilva@teste.com" 
        }

        cy.fillMandatoryFiedls(customer);

       

        cy.get('[type="submit"]').as("submitButton").should("not.be.disabled");
        cy.get('#agree').uncheck();
        cy.get('@submitButton').should("be.disabled");
    });

});